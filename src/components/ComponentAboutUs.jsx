import React from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap'
import imgadd1 from './CardImages/imgadd1.jpg'

export default function ComponentAboutUs() {
  const data =
    'The word nature is borrowed from the Old French nature and is derived from the Latin word natura, or "essential qualities, innate disposition", and in ancient times, literally meant "birth".[2] In ancient philosophy, natura is mostly used as the Latin translation of the Greek word physis (φύσις), which originally related to the intrinsic characteristics of plants, animals, and other features of the world to develop of their own accord.[3][4] The concept of nature as a whole, the physical universe.';
  return (
    <div>
      <Container>
        <div className="line1">
          <Row>
            <Col md={6}>
              <Card>
                <Card.Img variant="top" src={imgadd1} height="180px" />
              </Card>
            </Col>
            <Col md={6}>
              <Card style={{ height: "180px" }}>
                <Card.Body>{data}</Card.Body>
              </Card>
            </Col>
          </Row>
        </div>
        <div className="line2" style={{ marginTop: "10px" }}>
          <Row>
            <Col md={6}>
              <Card style={{ height: "180px" }}>
                <Card.Body>{data}</Card.Body>
              </Card>
            </Col>
            <Col md={6}>
              <Card>
                <Card.Img variant="top" src={imgadd1} height="180px" />
              </Card>
            </Col>
          </Row>
        </div>
        <div className="line3" style={{ marginTop: "10px" }}>
          <Row>
            <Col md={6}>
              <Card>
                <Card.Img variant="top" src={imgadd1} height="180px" />
              </Card>
            </Col>
            <Col md={6}>
              <Card style={{ height: "180px" }}>
                <Card.Body>{data}</Card.Body>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}
